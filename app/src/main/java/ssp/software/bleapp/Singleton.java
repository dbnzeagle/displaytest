package ssp.software.bleapp;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

public  class Singleton {
    public static Singleton singleton = new Singleton();
    public BluetoothGatt gattRec = null;
    public BluetoothGattCharacteristic dataRec=null;
}
