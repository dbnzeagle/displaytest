package ssp.software.bleapp;

import java.util.HashMap;

import android.util.SparseArray;

public class BleNamesResolver {
	private static HashMap<String, String> mServices = new HashMap<String, String>();
	private static HashMap<String, String> mCharacteristics = new HashMap<String, String>();
	private static SparseArray<String> mValueFormats = new SparseArray<String>();

	static public String resolveServiceName(final String uuid)
	{
		String result = mServices.get(uuid);
		if(result == null) result = "Unknown Service";
		return result;
	}

	static public String resolveValueTypeDescription(final int format)
	{
		Integer tmp = Integer.valueOf(format);
		return mValueFormats.get(tmp, "Unknown Format");
	}	
	
	static public String resolveCharacteristicName(final String uuid)
	{
		String result = mCharacteristics.get(uuid);
		if(result == null) result = "Unknown Characteristic";
		return result;
	}
	
	static public String resolveUuid(final String uuid) {
		String result = mServices.get(uuid);
		if(result != null) return "Service: " + result;
		
		result = mCharacteristics.get(uuid);
		if(result != null) return "Characteristic: " + result;
		
		result = "Unknown UUID";
		return result;
	}
	/*
	* Все UUID и названия параметров
	* */
	static {

		mServices.put("00001800-0000-1000-8000-00805f9b34fb", "Generic Access");

		mServices.put("4fafc201-1fb5-459e-8fcc-c5c9c331914b", "Display");

		mCharacteristics.put("62195ede-31f1-4507-ac31-37653c1f139c", "BLEVAL_FW_VER");
		mCharacteristics.put("dc5c04ee-832c-4d6e-a88d-3e626a25ded4", "BLEVAL_HW_VER");
		mCharacteristics.put("43c47b6e-33bc-4dbe-a8aa-85772b07a7f5", "BLEVAL_FG_COLOR");
		mCharacteristics.put("ec2fcc53-42e0-4e3f-aec3-49adffa5d398", "BLEVAL_BG_COLOR");
		mCharacteristics.put("637bd5f0-3205-4532-8cfc-edeab6c97e63", "BLEVAL_BRIGHTNESS");
		mCharacteristics.put("eedf8ba1-0fbf-4bbf-a28a-a730a29b7ed2", "BLEVAL_GAMMA");
		mCharacteristics.put("b9de57a9-3c4e-451e-895a-55e08d3b62d0", "BLEVAL_MODE");
		mCharacteristics.put("b64521c9-0ad2-4f37-9910-f88b2380de1b", "BLEVAL_SCROLLSPEED");
		mCharacteristics.put("f4da722f-2dcf-49dd-9317-b6e992b0884b", "BLEVAL_FREQ");
		mCharacteristics.put("a4ec761f-bbd3-4f15-aa7f-3c35462366f3", "BLEVAL_BITPLANES");
		mCharacteristics.put("cab84f8a-b224-484b-9e13-93fdd53093b4", "BLEVAL_FILENAME ");
		mCharacteristics.put("d96e6749-b601-4b02-b0bd-5f7b17273712", "BLEVAL_DEFAULTFILE");
		mCharacteristics.put("0ee4d2ae-203b-42ae-9c59-5b2f48806bfd", "BLEVAL_DEVICENAME");
		mCharacteristics.put("ef63dc5d-455d-49be-863c-b575adef71fb", "BLEVAL_SCROLLTEXT");
		mCharacteristics.put("182ea117-7877-4a15-8075-6ec527355351", "BLEVAL_SCROLLFONT");
		mCharacteristics.put("548d8c97-284b-48a1-a0f9-feef40523884", "BLEVAL_FOP_OPENFILE");
		mCharacteristics.put("defcecf4-8b7c-413c-88c8-4ba00c12938a", "BLEVAL_FOP_REMOVEFILE");
		mCharacteristics.put("51b64d7f-80dd-4c4f-aea6-9ab4c59f0b24", "BLEVAL_FOP_DATA");
		mCharacteristics.put("fac01041-0b9a-400a-9a2c-726c5b19d84e", "BLEVAL_FOP_POS");
		mCharacteristics.put("dbb8a3b1-e694-48b2-8e77-fd5cafe51efe", "BLEVAL_FOP_HASH");
		mCharacteristics.put("b2f52aac-55fd-42d2-ab83-17bb3b3a553a", "BLEVAL_FILE_COUNT");
		mCharacteristics.put("eea23eab-cb22-4061-9d2c-12ae2bef2237", "BLEVAL_FILE_INDEX");
		mCharacteristics.put("bba6ed5f-65a1-4256-973b-845cacd0b02e", "BLEVAL_FILE_NAME");
		mCharacteristics.put("8880f7bd-c968-41bb-9977-3cb3d7e204f6", "BLEVAL_OTA_WRITE");
		mCharacteristics.put("18d95c33-d3d9-474d-8b27-226007053fd3", "BLEVAL_FREE_HEAP");
		mCharacteristics.put("ad9f8e8c-4a76-4f84-9cfe-0b7e864398d3", "BLEVAL_CHIP_ID");
		mCharacteristics.put("67f35d72-6a95-4072-a8b6-6a28ec9159c6", "BLEVAL_CMD");

		mValueFormats.put(Integer.valueOf(52), "32bit float");
		mValueFormats.put(Integer.valueOf(50), "16bit float");
		mValueFormats.put(Integer.valueOf(34), "16bit signed int");
		mValueFormats.put(Integer.valueOf(36), "32bit signed int");
		mValueFormats.put(Integer.valueOf(33), "8bit signed int");
		mValueFormats.put(Integer.valueOf(18), "16bit unsigned int");
		mValueFormats.put(Integer.valueOf(20), "32bit unsigned int");
		mValueFormats.put(Integer.valueOf(17), "8bit unsigned int");
		

	}
}
