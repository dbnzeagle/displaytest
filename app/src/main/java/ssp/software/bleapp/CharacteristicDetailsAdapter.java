package ssp.software.bleapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Locale;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;


import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.github.ivbaranov.rxbluetooth.RxBluetooth;
import com.google.common.io.BaseEncoding;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;


public class CharacteristicDetailsAdapter extends BaseAdapter {

    private BluetoothGattCharacteristic mCharacteristic = null;
    private LayoutInflater mInflater;
    private BleWrapper mBleWrapper = null;
    private byte[] mRawValue = null;
    private BigInteger mIntValue ;
    private String mAsciiValue = "";
    private String mStrValue = "";
    private String mLastUpdateTime = "";
    private boolean mNotificationEnabled = true;
    private Context mContext=null;

    public CharacteristicDetailsAdapter(PeripheralActivity parent, BleWrapper ble,Context context) {
        super();
        mContext=context;
        mBleWrapper = ble;
        mInflater = parent.getLayoutInflater();
    }

    public void setCharacteristic(BluetoothGattCharacteristic ch) {
        this.mCharacteristic = ch;
        mRawValue = null;
        mIntValue = null;
        mAsciiValue = "";
        mStrValue = "";
        mLastUpdateTime = "-";
        mNotificationEnabled = false;
    }

    public BluetoothGattCharacteristic getCharacteristic(int index) {
        return mCharacteristic;
    }

    public void clearCharacteristic() {
        mCharacteristic = null;
    }

    @Override
    public int getCount() {
        return (mCharacteristic != null) ? 1 : 0;
    }

    @Override
    public Object getItem(int position) {
        return mCharacteristic;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void newValueForCharacteristic(final BluetoothGattCharacteristic ch, final String strVal, final BigInteger  intVal, final byte[] rawValue, final String timestamp) {
        if (!ch.equals(this.mCharacteristic)) return;

        mIntValue = intVal;
        mStrValue = strVal;
        mRawValue = rawValue;
        int i=0;
        if (mRawValue != null && mRawValue.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(mRawValue.length);
            for(byte byteChar : mRawValue)
                stringBuilder.append(String.format("%02X", byteChar));
            mAsciiValue =stringBuilder.toString();
        } else mAsciiValue = "";

        mLastUpdateTime = timestamp;
        if (mLastUpdateTime == null) mLastUpdateTime = "";
    }

    public void setNotificationEnabledForService(final BluetoothGattCharacteristic ch) {
        if ((!ch.equals(this.mCharacteristic)) || (mNotificationEnabled == true)) return;
        mNotificationEnabled = true;
        notifyDataSetChanged();
    }
    public byte[] parseGifToByte() throws IOException {

            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"dislike.gif");
        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        long length = file.length();
            if (length > Integer.MAX_VALUE) {
                // File is too large
            }
            byte[] bytes = new byte[(int)length];

            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length
                    && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }


        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }
    public byte[] parseHexStringToBytes(final String hex,BluetoothGattCharacteristic mCharacteristic) {
        String tmp=hex;
        if(hex.length()%2!=0) {
            tmp = "";
            tmp = hex + '0';
        }

        byte[] reversedBytes=BaseEncoding.base16().decode(tmp.toUpperCase());
        byte[] bytes = BaseEncoding.base16().decode(tmp.toUpperCase());
        if(!mCharacteristic.getUuid().toString().equals("ef63dc5d-455d-49be-863c-b575adef71fb")) {
            for (int i = 1; i <= bytes.length; i++) {
                reversedBytes[i - 1] = bytes[bytes.length - i];
            }
        }
        return reversedBytes;
    }
    public byte[] parseTextToByte(final String text,BluetoothGattCharacteristic mCharacteristic) {
        byte[] b = text.getBytes();
        return b;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup p) {
        FieldReferences fields;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.peripheral_details_characteristic_item, null);
            fields = new FieldReferences();
            fields.charPeripheralName = (TextView) convertView.findViewById(R.id.char_details_peripheral_name);
            fields.charPeripheralAddress = (TextView) convertView.findViewById(R.id.char_details_peripheral_address);
            fields.charServiceName = (TextView) convertView.findViewById(R.id.char_details_service);
            fields.charServiceUuid = (TextView) convertView.findViewById(R.id.char_details_service_uuid);
            fields.charName = (TextView) convertView.findViewById(R.id.char_details_name);
            fields.charUuid = (TextView) convertView.findViewById(R.id.char_details_uuid);

            fields.charDataType = (TextView) convertView.findViewById(R.id.char_details_type);
            fields.charProperties = (TextView) convertView.findViewById(R.id.char_details_properties);
            fields.charDecValue=(TextView) convertView.findViewById(R.id.char_details_dec_value);
            fields.charStrValue = (TextView) convertView.findViewById(R.id.char_details_ascii_value);
            fields.charHexValue = (EditText) convertView.findViewById(R.id.char_details_hex_value);
            fields.charDateValue = (TextView) convertView.findViewById(R.id.char_details_timestamp);

            fields.notificationBtn = (ToggleButton) convertView.findViewById(R.id.char_details_notification_switcher);
            fields.readBtn = (Button) convertView.findViewById(R.id.char_details_read_btn);
            fields.writeBtn = (Button) convertView.findViewById(R.id.char_details_write_btn);
            fields.writeBtn.setTag(fields.charHexValue);

            fields.readBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBleWrapper.requestCharacteristicValue(mCharacteristic);
                }
            });

            fields.writeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText hex = (EditText) v.getTag();
                    byte[] dataToWrite = new byte[0];
                    String newValue = hex.getText().toString().toLowerCase(Locale.getDefault());
                    //byte[] dataToWrite = parseHexStringToBytes(newValue,mCharacteristic);
                    if(mCharacteristic.getUuid().toString().equals("548d8c97-284b-48a1-a0f9-feef40523884")
                            ||mCharacteristic.getUuid().toString().equals("d96e6749-b601-4b02-b0bd-5f7b17273712")
                            ||mCharacteristic.getUuid().toString().equals("defcecf4-8b7c-413c-88c8-4ba00c12938a")
                            ||mCharacteristic.getUuid().toString().equals("d96e6749-b601-4b02-b0bd-5f7b17273712")
                            ||mCharacteristic.getUuid().toString().equals("ef63dc5d-455d-49be-863c-b575adef71fb")) {
                        dataToWrite = parseTextToByte(newValue, mCharacteristic);
                        mBleWrapper.writeDataToCharacteristic(mCharacteristic, dataToWrite);
                    }
                    else if(mCharacteristic.getUuid().toString().equals("51b64d7f-80dd-4c4f-aea6-9ab4c59f0b24")) {

                        Intent intent = new Intent();
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        // Set your required file type
                        intent.setType("*/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        ((PeripheralActivity) mContext).startActivityForResult(intent, 1);
                        Singleton.singleton.dataRec=mCharacteristic;
                      /*  try {
                            dataToWrite=parseGifToByte();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mBleWrapper.writeDataToCharacteristic(mCharacteristic, dataToWrite);*/

                    }
                    else{
                        dataToWrite=parseHexStringToBytes(newValue,mCharacteristic);
                        mBleWrapper.writeDataToCharacteristic(mCharacteristic, dataToWrite);
                    }


                }
            });
            fields.notificationBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked == mNotificationEnabled) return; // no need to update anything

                    mBleWrapper.setNotificationForCharacteristic(mCharacteristic, isChecked);
                    mNotificationEnabled = isChecked;
                }
            });

            convertView.setTag(fields);
        } else {
            fields = (FieldReferences) convertView.getTag();
        }

        // set proper values into the view
        fields.charPeripheralName.setText(mBleWrapper.getDevice().getName());
        fields.charPeripheralAddress.setText(mBleWrapper.getDevice().getAddress());

        String tmp = mCharacteristic.getService().getUuid().toString().toLowerCase(Locale.getDefault());
        fields.charServiceUuid.setText(tmp);
        fields.charServiceName.setText(BleNamesResolver.resolveServiceName(tmp));

        String uuid = mCharacteristic.getUuid().toString().toLowerCase(Locale.getDefault());
        String name = BleNamesResolver.resolveCharacteristicName(uuid);

        fields.charName.setText(name);
        fields.charUuid.setText(uuid);

        int format = mBleWrapper.getValueFormat(mCharacteristic);
        fields.charDataType.setText(BleNamesResolver.resolveValueTypeDescription(format));
        int props = mCharacteristic.getProperties();
        String propertiesString = String.format("0x%04X [", props);
        if ((props & BluetoothGattCharacteristic.PROPERTY_READ) != 0) propertiesString += "read ";
        if ((props & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0) propertiesString += "write ";
        if ((props & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0)
            propertiesString += "notify ";
        if ((props & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0)
            propertiesString += "indicate ";
        if ((props & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0)
            propertiesString += "write_no_response ";
        fields.charProperties.setText(propertiesString + "]");

        fields.notificationBtn.setEnabled((props & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0);
        fields.notificationBtn.setChecked(mNotificationEnabled);
        fields.readBtn.setEnabled((props & BluetoothGattCharacteristic.PROPERTY_READ) != 0);
        fields.writeBtn.setEnabled((props & (BluetoothGattCharacteristic.PROPERTY_WRITE | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) != 0);
        fields.charHexValue.setEnabled(fields.writeBtn.isEnabled());

        fields.charHexValue.setText(mAsciiValue);
        fields.charStrValue.setText(mStrValue);
        fields.charDecValue.setText(String.format("%d", mIntValue));
        fields.charDateValue.setText(mLastUpdateTime);

        return convertView;
    }

    private class FieldReferences {
        TextView charPeripheralName;
        TextView charPeripheralAddress;
        TextView charServiceName;
        TextView charServiceUuid;
        TextView charUuid;
        TextView charName;
        TextView charDataType;
        TextView charStrValue;
        EditText charHexValue;
        TextView charDecValue;
        TextView charDateValue;
        TextView charProperties;

        ToggleButton notificationBtn;
        Button readBtn;
        Button writeBtn;
    }
}
